#include <memory.h>
#include <stdio.h>
#include "rom.h"

int pointers[28] = {
	0x1008C, 0x1008A, 0x1008E, 0x10090, 0x10072, 0x10074, 0x10076,
	0x10078, 0x1007A, 0x1007C, 0x1007E, 0x10092, 0x10094, 0x10084,
	0x1009A, 0x10088, 0x10096, 0x10098, 0x10086, 0x1009C, 0x1009E,
	0x100A0, 0x100A2, 0x100A4, 0x100AC, 0x100A8, 0x100AA, 0x100A6
};

ROM *rom_init(void)
{
	ROM *rom = malloc(sizeof(ROM));
	rom->filename = NULL;	
	rom->team = NULL;
	rom->pictures = NULL;	
	rom->tiles = malloc(524288);	
	return rom;
}

PIC *rom_add_picture(ROM *rom)
{
	PIC *new_pic = malloc(sizeof(PIC));

	new_pic->next = rom->pictures;
	rom->pictures = new_pic;

	return new_pic;
}

void rom_destroy_pictures(ROM *rom)
{
	PIC *pic = rom->pictures;
	while (pic)
	{
		PIC *tmp = pic;
		pic = pic->next;
		free(tmp);
	}
	
	rom->pictures = NULL;
}

void rom_destroy_helmets(ROM *rom)
{
	int i;
	for (i=0; i<4; i++)
	{		
		helmet_destroy(rom->helmets[i]);
	}
}

int rom_load(ROM *rom, const char *filename)
{
	ALLEGRO_FILE *fp;
	int i=0,j=0;
	unsigned char tile[0x10];	

	rom->filename = malloc(strlen(filename) + 1);
	if (!rom->filename) return 1;	
	strcpy(rom->filename, filename);

	if (!(fp = al_fopen(filename, "rb")))
	{		
		return 1;
	}
	else
	{		
        HELMET *helmet;
		GFX_GRID *grid_br = gfx_grid_create(0x165c7, 4,4);

		// 43: bengals
		helmet = rom->helmets[3] = helmet_create();
		helmet_add_grid(helmet, gfx_grid_create(0x1664F, 4, 4), 0, 0);
		helmet_add_grid(helmet, gfx_grid_create(0x16572, 4, 4), 4, 0);
		helmet_add_grid(helmet, gfx_grid_create(0x16583, 4, 4), 0, 4);
		helmet_add_grid(helmet, grid_br, 4, 4);
		
		// 42: rams
		helmet = rom->helmets[2] = helmet_create();
		helmet_add_grid(helmet, gfx_grid_create(0x1660b, 4, 4), 0, 0);
		helmet_add_grid(helmet, gfx_grid_create(0x1661c, 4, 4), 4, 0);
		helmet_add_grid(helmet, gfx_grid_create(0x1662d, 4, 4), 0, 4);
		helmet_add_grid(helmet, gfx_grid_create(0x1663e, 4, 4), 4, 4);
		
		// 41: no stripe
		helmet = rom->helmets[1] = helmet_create();
		helmet_add_grid(helmet, gfx_grid_create(0x165d8, 4, 4), 0, 0);
		helmet_add_grid(helmet, gfx_grid_create(0x165e9, 4, 4), 4, 0);
		helmet_add_grid(helmet, gfx_grid_create(0x165fa, 4, 4), 0, 4);
		helmet_add_grid(helmet, grid_br, 4, 4);
		
		// 40: striped
		helmet = rom->helmets[0] = helmet_create();
		helmet_add_grid(helmet, gfx_grid_create(0x16594, 4, 4), 0, 0);
		helmet_add_grid(helmet, gfx_grid_create(0x165a5, 4, 4), 4, 0);
		helmet_add_grid(helmet, gfx_grid_create(0x165b6, 4, 4), 0, 4);
		helmet_add_grid(helmet, grid_br, 4, 4);

		gfx_grid_load_all(fp);
	}


	rom->team_count = 28;
	rom->team = malloc(sizeof(TEAM) * rom->team_count);

	while ( i < rom->team_count)
	{
		rom->team[i].address = pointers[i];
		++i; 
	}

	for (i=0; i<rom->team_count; i++)
	{
		rom->team[i].logo = rom_add_picture(rom);

		al_fseek(fp, rom->team[i].address, ALLEGRO_SEEK_SET);
		rom->team[i].logo->hdr_addr = (uint16_t) al_fread16le(fp);
	}

	al_fseek(fp, 0x23D79, ALLEGRO_SEEK_SET);
	for (i=0; i<rom->team_count; i++)
	{
		al_fread(fp, rom->team[i].palette, 8);		
	}

	// load base helmet numbers
	al_fseek(fp, 0x23E59, ALLEGRO_SEEK_SET);
	for (i=0; i<rom->team_count; i++)
		rom->team[i].helmet_num = al_fgetc(fp);
		

	for (i=0; i<rom->team_count; i++)
	{
		al_fseek(fp, rom->team[i].logo->hdr_addr + 0x6010, ALLEGRO_SEEK_SET);

		rom->team[i].logo->hdr.junk0 = al_fgetc(fp);
		rom->team[i].logo->hdr.pt_num = al_fgetc(fp);
		al_fread(fp, rom->team[i].logo->hdr.junk1, 6);
		rom->team[i].logo->hdr.y = al_fgetc(fp);
		rom->team[i].logo->hdr.x = al_fgetc(fp);
		rom->team[i].logo->hdr.design_addr = al_fread16be(fp);
		al_fread(fp, rom->team[i].logo->hdr.junk2, 2);
/*		
		printf("%02X %02X %02X%02X%02X %02X%02X%02X %02X %02X %04X %02X%02X\n", 
			rom->team[i].logo->hdr.junk0,
			rom->team[i].logo->hdr.pt_num,
			rom->team[i].logo->hdr.junk1[0],rom->team[i].logo->hdr.junk1[1],rom->team[i].logo->hdr.junk1[2],rom->team[i].logo->hdr.junk1[3],rom->team[i].logo->hdr.junk1[4],rom->team[i].logo->hdr.junk1[5],
			rom->team[i].logo->hdr.y, rom->team[i].logo->hdr.x, 
			rom->team[i].logo->hdr.design_addr, 
			rom->team[i].logo->hdr.junk2[0], rom->team[i].logo->hdr.junk2[1]
		);
*/		
		if (rom->team[i].logo->hdr.pt_num > 128)
		{
			printf("Invalid CHR page found. Defaulting to 0.\n");
			rom->team[i].logo->hdr.pt_num = 0;
		}
		rom->team[i].logo->chr = rom->tiles + (rom->team[i].logo->hdr.pt_num * 4096);
	}

	for (i=0; i<rom->team_count; i++)
	{		
		al_fseek(fp, rom->team[i].logo->hdr.design_addr + 0x8010, ALLEGRO_SEEK_SET);
		j = 0;		
		while ((rom->team[i].logo->data[j++] = al_fgetc(fp)) != 0xff && j < 2047);
		if (j >= 2048) return 1;
		
		//printf("%0x - %0x (%d)\n", team[i].logo->hdr.design_addr + 0x8010, team[i].logo->hdr.design_addr + 0x8010 + j, j);
	}

	al_fseek(fp, 0x1FD00, ALLEGRO_SEEK_SET);
	for (i=0; i<rom->team_count; i++)
	{
		al_fgets(fp, rom->team[i].name, 5);
	}

	for (i=0; i<rom->team_count; i++)
	{
		rom->team[i].num = i;
		rom->team[i].bmp_helmet = al_create_bitmap(64,64);
	}

	{
		unsigned char *foo = rom->tiles;
		
		al_fseek(fp, 0x40010, ALLEGRO_SEEK_SET);  //0x5C010 is where helmets start
		while (al_fread(fp, tile, 0x10) == 0x10)
		{	
			for (i=0; i<8; i++)
			{
				for (j=0; j<8; j++)  
				{					
					*(foo++) = ((tile[i] & (1<<(7-j))) ? 1 : 0) + ((tile[i+8] & (1<<(7-j))) ? 2 : 0);					
				}
			}	
		}		
	}
	
	al_fclose(fp);
    
	printf("Loaded File.\n");

	return 0;
}

void rom_save(ROM *rom)
{
	ALLEGRO_FILE *fp;
	
   	if ((fp = al_fopen(rom->filename, "r+b")))
	{
		int i,j; //,k,l;		

		/* save the code first, so we know what to point to */
		al_fseek(fp, 0x1119D, ALLEGRO_SEEK_SET);
		for (i=0; i<rom->team_count; i++)
		{		
			rom->team[i].logo->hdr.design_addr = al_ftell(fp) - 0x8010;			
			j = 0;
			while (rom->team[i].logo->data[j] != 0xff) al_fputc(fp, rom->team[i].logo->data[j++]);
			al_fputc(fp, 0xFF);
		}

		// update the team logo headers
		for (i=0; i<rom->team_count; i++)
		{			
			al_fseek(fp, rom->team[i].logo->hdr_addr + 0x6010, ALLEGRO_SEEK_SET);
		
			al_fputc(fp, rom->team[i].logo->hdr.junk0);
			al_fputc(fp, rom->team[i].logo->hdr.pt_num);
			al_fwrite(fp, rom->team[i].logo->hdr.junk1, 6);
			al_fputc(fp, rom->team[i].logo->hdr.y);
			al_fputc(fp, rom->team[i].logo->hdr.x);
			al_fwrite16be(fp, rom->team[i].logo->hdr.design_addr);
			al_fwrite(fp, rom->team[i].logo->hdr.junk2, 2);
		}

		{
			int tn;
			unsigned char tile[0x10];
			unsigned char *foo = rom->tiles;

			al_fseek(fp, 0x40010, ALLEGRO_SEEK_SET);  //0x5C010 is where helmets start
			for (tn=0; tn<8192; tn++)
			{					
				memset(tile, 0, 0x10);
				for (i=0; i<8; i++)
				{
					for (j=0; j<8; j++)  
					{					
						tile[i] |= (*foo & 1) << (7-j);
						tile[i+8] |= ((*foo & 2)>>1) << (7-j);

						foo++;
					}
				}
				al_fwrite(fp, tile, 0x10);
			}			
		}
		
		al_fclose(fp);
		printf("saved\n");	
	}
}

void rom_destroy(ROM *rom)
{
	if (rom->filename)
	{
		free(rom->filename);
		rom->filename = 0;
	}
	if (rom->team)
	{
		free(rom->team);
		rom->team = 0;
	}	
	if (rom->tiles)
	{
		free(rom->tiles);
		rom->tiles = 0;
	}
	rom_destroy_pictures(rom);
	rom_destroy_helmets(rom);

	gfx_grid_clear();

	return;
}
