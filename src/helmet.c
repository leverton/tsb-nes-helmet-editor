#include <stdio.h>
#include <memory.h>
#include <allegro5/allegro.h>

#include "helmet.h"
#include "gfx.h"

GFX_GRID *gfx_grids = NULL;

HELMET *helmet_create(void)
{
	HELMET *h = malloc(sizeof(HELMET));

	if (h)
	{
		h->grid_links = NULL;		
	}

	return h;
}

void helmet_destroy(HELMET *helmet)
{
	GFX_GRID_LINK *grid_link = helmet->grid_links;
	while (grid_link)
	{
		GFX_GRID_LINK *tmp = grid_link;
		grid_link = grid_link->next;		
		free(tmp);
		printf("Destroyed Grid Link %p\n", tmp);
	}

	helmet->grid_links = 0;
}

GFX_GRID *gfx_grid_create(int address, int w, int h)
{
	GFX_GRID *gfx_grid = malloc(sizeof(GFX_GRID));

    gfx_grid->address = address;
	gfx_grid->w = w;
	gfx_grid->h = h;
	
	gfx_grid->data = malloc(w * h);

	gfx_grid->next = gfx_grids;
	gfx_grids = gfx_grid;

	printf("Created Grid %0x (%p)\n", address, gfx_grid);

	return gfx_grid;
}

void gfx_grid_clear()
{
	GFX_GRID *gfx_grid = gfx_grids;
	while (gfx_grid)
	{
		GFX_GRID *tmp = gfx_grid;
		gfx_grid = gfx_grid->next;
		free(tmp->data);
		free(tmp);
		printf("Destroyed Grid %p\n", tmp);
	}

	gfx_grids = 0;
}

void helmet_add_grid(HELMET *helmet, GFX_GRID *grid, int x, int y)
{
	GFX_GRID_LINK *grid_link = malloc(sizeof(GFX_GRID_LINK));

    grid_link->x = x;
	grid_link->y = y;
	grid_link->grid = grid;

	grid_link->next = helmet->grid_links;
	helmet->grid_links = grid_link;

	printf("Created Grid Link %p\n", grid_link);
}

void gfx_grid_load_all(ALLEGRO_FILE *fp)
{
	GFX_GRID *gd = gfx_grids;
	while (gd)
	{
		al_fseek(fp, gd->address, SEEK_SET);
		al_fread(fp, gd->data, gd->w * gd->h);

		gd = gd->next;
	}
}

void helmet_draw(HELMET *helmet, unsigned char *tiles, int base_c, ALLEGRO_BITMAP *bmp, int dest_x, int dest_y, int zoom)
{
	GFX_GRID_LINK *grid_link = helmet->grid_links;	
	
	while (grid_link)
	{
		int x, y, i = 0;
		for (y=0; y<grid_link->grid->h; y++)
			for (x=0; x<grid_link->grid->w; x++)
			{
				draw_gfx(tiles + (grid_link->grid->data[i++]) * 64, 8,8, 0, base_c, bmp, dest_x + (grid_link->x + x) * 8 * zoom, dest_y + (grid_link->y + y) * 8 * zoom, zoom);
			}

		grid_link = grid_link->next;
	}
}

bool helmet_putpixel(HELMET *helmet, unsigned char *tiles, int x, int y, char c)
{
	GFX_GRID_LINK *grid_link = helmet->grid_links;
	while (grid_link)
	{
		if (
			x >= grid_link->x*8 && x < (grid_link->x + grid_link->grid->w) * 8 &&
			y >= grid_link->y*8 && y < (grid_link->y + grid_link->grid->h) * 8
			)
		{
			int tile_num = 0, row, col;
			x -= grid_link->x*8;
			y -= grid_link->y*8;

			row = y / 8;
			col = x / 8;

			y %= 8;
			x %= 8;
            
			tile_num = grid_link->grid->data[row * grid_link->grid->w + col];
			
			*(tiles + (tile_num * 64) + (y * 8) + x) = c;

			printf("%0x: %d,%d (%d)\n", tile_num, x, y,c);

			return true;
		}
		grid_link = grid_link->next;
	}

	return false;
}
