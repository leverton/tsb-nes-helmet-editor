#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_native_dialog.h>

#include "gfx.h"
#include "picture.h"
#include "helmet.h"
#include "team.h"
#include "rom.h"

#define between_coords(x,y, x1,y1, x2,y2) ((x) >= (x1) && (x) <= (x2) && (y) >= (y1) && (y) <= (y2))

#define PEN_OFF		0
#define PEN_DRAW	1
#define PEN_ERASE	2

#define TOOL_DRAW	1
#define TOOL_CLEAR	2
#define TOOL_REMOVE	3
#define TOOL_MOVE	4

#define MOVE_NORMAL	1
#define MOVE_OFFSET 2

#define LAYER_HELMET 1
#define LAYER_LOGO   2

#define SCREEN_W 640
#define SCREEN_H 480


static ROM *rom;

static ALLEGRO_COLOR pal[256];
static ALLEGRO_COLOR pal_nes[256];
static ALLEGRO_COLOR pal_edit[256];

static ALLEGRO_COLOR *active_pal = pal;

static ALLEGRO_FONT *font;
static ALLEGRO_DISPLAY *display;
 // COLOR_MAP cm[16];
static ALLEGRO_MOUSE_CURSOR *cur_draw;
static ALLEGRO_BITMAP *bmp_cur_draw;

static float scale_x = 1;
static float scale_y = 1;
static int offset_x = 0;
static int offset_y = 0;

// Full screen mode is resolution independent, which makes
// switching in and out fast, but means we must set up a 
// scale to stretch the 640x480 based drawing into whatever
// resolution the full screen is.
//
static void scale_screen(int bw, int bh, int dw, int dh)
{
  ALLEGRO_TRANSFORM t;

  // Calculate the horizontal and vertial aspect ratios
  const float HAR = dw/(float)bw;
  const float VAR = dh/(float)bh;  
  
  // The aspect ratio, x-offset and y-offset (in pixels)
  float ar, ox, oy;
  
  if (bw == dw && bh == dh)
  {
	// 1:1, just reset everything
	al_identity_transform(&t);
	al_use_transform(&t);
	al_set_clipping_rectangle(0, 0, bw, bh);
	scale_x = 1;
	scale_y = 1;
	offset_x = 0;
	offset_y = 0;
  }
  else
  {
	// Choose the smaller aspect ratio    
	if (HAR < VAR)
	{
	  // horizontal bars on the top and bottom
	  ar = HAR;
	  ox = 0;
	  oy = (dh - (ar * bh)) / 2.0;
	}
	else
	{
	  // vertical bars on the left and right
	  ar = VAR;
	  ox = (dw - (ar * bw)) / 2.0;
	  oy = 0;
	}

	// set the global scale/offset so the mouse coords can be inverted
	scale_x = ar;
	scale_y = ar;
	offset_x = ox;
	offset_y = oy;

	// set up the transformation to scale and translate
	al_build_transform(&t, ox, oy, ar, ar, 0);
	al_use_transform(&t);

	// clear out the screen before setting the clipping
	al_set_clipping_rectangle(0, 0, dw, dh);
	al_clear_to_color(al_map_rgb(0,0,0));
	al_flip_display();

	// make sure nothing is drawn into the black bars 
	al_set_clipping_rectangle(ox, oy, ar * bw, ar * bh);
  }
}

char *init_dir = NULL;

void draw_gfx(const unsigned char *img, int w, int h, unsigned char mask_c, int base_c, ALLEGRO_BITMAP *bmp, int x, int y, int zoom)
{
	int a,b;

	ALLEGRO_BITMAP *prev = al_get_target_bitmap();

	al_set_target_bitmap(bmp);

	for (a = 0; a<h; a++)
	{
		for (b=0; b<w; b++)
		{			
			int c = img[a*w + b];
			if (c != mask_c) 
			{
				if (zoom == 1)
					al_draw_pixel(x+b,y+a, active_pal[c + base_c]);
				else			
					al_draw_filled_rectangle(
						x + (b * zoom), y + (a * zoom),
						x + ((b + 1) * zoom), y + ((a + 1) * zoom),
						active_pal[c + base_c]
					);
			}
		}
	}

	al_set_target_bitmap(prev);	
}

void draw_logo(PIC *logo, int base_color, ALLEGRO_BITMAP *bmp, int x, int y, int zoom)
{
	GRID *grid = &(logo->grid);
	int i;

	for (i = 0; i<grid->tile_count; i++)
	{
		GRID_TILE *gt = &(grid->tile[i]);
		draw_gfx(
			logo->chr + (gt->num * 64),
			8,
			8,
			0,
			base_color,
			bmp,
			x + (grid->x + gt->x + gt->col*8) * zoom,
			y + (grid->y + gt->y + gt->row*8) * zoom,
			zoom
		);			
	}  	
}

void draw_helmet(TEAM *t)
{
	al_set_target_bitmap(t->bmp_helmet);
	al_clear_to_color(al_map_rgba(0,0,0,0));
	helmet_draw(rom->helmets[t->helmet_num-0x40], rom->tiles + 40*4096, 0x0F + t->num*8, t->bmp_helmet, 0,0, 1);
	draw_logo(t->logo, 0x14 + t->num*8, t->bmp_helmet, 0,0, 1);
	al_set_target_backbuffer(display);
	return;
}




char filename[512];

static bool select_file(char *filename)
{
	bool success = false;
	ALLEGRO_FILECHOOSER *fc = al_create_native_file_dialog(
		NULL, "Open File", "*.nes;*.*", ALLEGRO_FILECHOOSER_FILE_MUST_EXIST
	);

	if (al_show_native_file_dialog(NULL, fc) && al_get_native_file_dialog_path(fc, 0))
	{
		strncpy(filename, al_get_native_file_dialog_path(fc, 0), 511);
		filename[511] = 0;
		success = true;
	}

	al_destroy_native_file_dialog(fc);

	return success;
}

static void fade_out()
{
	ALLEGRO_BITMAP *backbuffer = al_get_backbuffer(display);
	ALLEGRO_BITMAP *bmp = al_create_bitmap(al_get_bitmap_width(backbuffer), al_get_bitmap_height(backbuffer));

	ALLEGRO_TIMER *timer = al_create_timer(1/120.0);

	ALLEGRO_TRANSFORM t;

	int64_t pc = -1;

	al_set_target_bitmap(bmp);
	al_draw_bitmap(backbuffer, 0, 0, 0);

	al_set_target_bitmap(backbuffer);

	al_identity_transform(&t);
	al_use_transform(&t);
	al_set_clipping_rectangle(0, 0, al_get_bitmap_width(backbuffer), al_get_bitmap_height(backbuffer));

	al_start_timer(timer);

	while (true)
	{
		int64_t c = al_get_timer_count(timer);
		if (c == pc)
		{
			al_rest(0.001);
		}
		else
		{
			float a = 1 - c / 60.0;
			pc = c;

			if (c >= 60) break;

			al_clear_to_color(al_map_rgb(0,0,0));
			al_draw_tinted_bitmap(bmp, al_map_rgb_f(a, a, a), 0, 0, 0);
			
			al_flip_display();
		}
	}

	al_destroy_bitmap(bmp);
	al_destroy_timer(timer);

	al_clear_to_color(al_map_rgb(0,0,0));
	al_flip_display();

	scale_screen(SCREEN_W, SCREEN_H, al_get_bitmap_width(backbuffer), al_get_bitmap_height(backbuffer));
}

int select_team(void)
{
	ALLEGRO_EVENT_QUEUE *queue;
	ALLEGRO_EVENT event;
	ALLEGRO_TIMER *timer;

	int x=-1,y=-1;
	int hover_team = -1;
	int status = 0;
	int i,j;
	int mouse_x = 0, mouse_y = 0;

	bool link_cursor = false;

	for (i=0; i<rom->team_count; i++)
	{
		for (j=0; j<8; j++)
		{
			int c = 0x10 + i * 8 + j;
			pal[c] = pal_nes[rom->team[i].palette[j]];
		}
	}

	active_pal = pal;
	
	for (i=0; i<rom->team_count; i++)
	{
		pic_code_to_grid(rom->team[i].logo);
		draw_helmet(&(rom->team[i]));
	}

	timer = al_create_timer(1 / 60.0);

	queue = al_create_event_queue();
	al_register_event_source(queue, al_get_display_event_source(display));
	al_register_event_source(queue, al_get_keyboard_event_source());
	al_register_event_source(queue, al_get_mouse_event_source());
	al_register_event_source(queue, al_get_timer_event_source(timer));

	al_start_timer(timer);
	
	while (status != 666)
	{
		do
		{
			al_wait_for_event(queue, &event);
			switch (event.type)
			{
				case ALLEGRO_EVENT_MOUSE_AXES:
					mouse_x = (event.mouse.x - offset_x) / scale_x;
					mouse_y = (event.mouse.y - offset_y) / scale_y;

					x = mouse_x - 50; y = mouse_y - 100;
					if (x >= 0 && x < 560 && y>=0 && y < 320)
					{
						int t = (y / 80)*7 + (x / 80);
						int ox = x-((t%7)*80);
						int oy = y-((t/7)*80);

						if (ox >= 60 || oy >= 60) t = -1;

						if (hover_team != t)
						{
							hover_team = t;
						}
					}
					else if (hover_team != -1)
					{
						hover_team = -1;
					}
					break;

				case ALLEGRO_EVENT_KEY_UP:
					if (event.keyboard.keycode == ALLEGRO_KEY_ESCAPE)
					{
						hover_team = -1;
						status = 666;
					}
					else if (event.keyboard.keycode == ALLEGRO_KEY_F10)
					{
						al_clear_to_color(al_map_rgb(0,0,0));
						al_flip_display();
						al_toggle_display_flag(display, ALLEGRO_FULLSCREEN_WINDOW, !(al_get_display_flags(display) & ALLEGRO_FULLSCREEN_WINDOW));
						scale_screen(SCREEN_W, SCREEN_H, al_get_display_width(display), al_get_display_height(display));						
					}
					break;

				case ALLEGRO_EVENT_DISPLAY_CLOSE:
					hover_team = -1;
					status = 666;
					break;

				case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
					if (hover_team != -1) status = 666;
					break;
			}

			if (link_cursor && hover_team == -1)
			{
				al_set_system_mouse_cursor(display, ALLEGRO_SYSTEM_MOUSE_CURSOR_DEFAULT);
				link_cursor = false;
			}
			else if (!link_cursor && hover_team != -1)
			{
				al_set_system_mouse_cursor(display, ALLEGRO_SYSTEM_MOUSE_CURSOR_LINK);
				link_cursor = true;
			}
		}
		while (!status && !al_is_event_queue_empty(queue));

		al_clear_to_color(al_map_rgb(0,0,0));
		al_draw_textf(
			font,
			al_map_rgb(255,255,255),
			SCREEN_W / 2 - al_get_text_width(font, "SELECT A TEAM") / 2,
			40 - al_get_font_line_height(font) / 2,
			ALLEGRO_ALIGN_LEFT,
			"SELECT A TEAM"
		);
		al_draw_line(0, 79.5, SCREEN_W, 79.5, active_pal[12], 1);  
		al_draw_line(0, 419.5, SCREEN_W, 419.5, active_pal[12], 1); 
					
		for (i=0; i<rom->team_count; i++)
		{	
			if (hover_team != i && mouse_y >= 80 && mouse_y <= 420)
				al_draw_tinted_bitmap(rom->team[i].bmp_helmet, al_map_rgba(30,30,30,30), 50 + (i % 7) * 80, 100 + (i / 7) * 80, 0);
			else				
				al_draw_bitmap(rom->team[i].bmp_helmet, 50 + (i % 7) * 80, 100 + (i / 7) * 80, 0);
		}

		al_flip_display();
	}

	if (link_cursor)
		al_set_system_mouse_cursor(display, ALLEGRO_SYSTEM_MOUSE_CURSOR_DEFAULT);

	al_destroy_timer(timer);
	al_destroy_event_queue(queue);

	fade_out();

	return hover_team;
}

int get_xy_coord_tile_num(GRID *grid, int mx, int my, int *tx, int *ty, int *tile_index)
{
	if (between_coords(mx,my, 20,100, 260,340))
	{
		int i = grid->tile_count;
		mx -= 20; my -= 100;
		while (i--)
		{
			int t_ox = (grid->x + grid->tile[i].x + grid->tile[i].col * 8) * 4;
			int t_oy = (grid->y + grid->tile[i].y + grid->tile[i].row * 8) * 4;
			
			if (mx >= t_ox && mx < t_ox+32 && my >= t_oy && my < t_oy+32)
			{				
				if (tx) *tx = (mx - t_ox) / 4;
				if (ty) *ty = (my - t_oy) / 4;
				if (tile_index) *tile_index = i;
				return grid->tile[i].num;			
			}
		}
	}
	else if (between_coords(mx,my, 356,139, 620,403))
	{
		mx -= 356; my -= 139;
		if ((my % 33) && (mx % 33)) // skip the blank border 
		{
			int t_ox = (mx / 33) * 33 + 1; // need to add 1px because of the blank border on the top / left
			int t_oy = (my / 33) * 33 + 1;

			if (tx) *tx = (mx - t_ox) / 4;
			if (ty) *ty = (my - t_oy) / 4;
			if (tile_index) *tile_index = -1;

			return (mx / 33) + (my / 33)*8;
		}
	}

	return -1;
}

TEAM *team;

static int do_init()
{
	ALLEGRO_FILE *fp;
	int i;
	ALLEGRO_PATH *path;

	al_init_primitives_addon();
	al_install_keyboard();
	al_install_mouse();
	al_init_font_addon();
	al_init_ttf_addon();

	display = al_create_display(640, 480);

	scale_screen(640, 480, al_get_display_width(display), al_get_display_height(display));

	al_set_window_title(display, "Helmet Editor for TSB");

	path = al_get_standard_path(ALLEGRO_RESOURCES_PATH);
	al_change_directory(al_path_cstr(path, '/'));
	al_destroy_path(path);

	font = al_load_ttf_font("data/Ubuntu-L.ttf", -16, 0);

	/* load palette */
	if (!(fp = al_fopen("data/nes.pal", "rb")))
	{
		printf("Unable to open data/nes.pal\n\n");
		exit(1);
	}
	for (i=0; i<64; i++)
	{
		const int r = al_fgetc(fp);
		const int g = al_fgetc(fp);
		const int b = al_fgetc(fp);
		pal_nes[i] = al_map_rgb(r, g, b);
	}
	al_fclose(fp);

	for (i=0; i<16; i++)
	{
		pal_edit[i] = pal[i] = al_map_rgb_f(0, 0.75 * i/15.0, 0);
	}

	for (i=0; i<8; i++)
	{
		pal_edit[i + 240] = pal[i + 240] = al_map_rgb_f(0.37 * i/7.0, 0, 0);
	}

	for (i=0; i<8; i++)
	{
		const float c = i / 7.0;
		pal_edit[i + 248] = pal[i + 248] = al_map_rgb_f(c, c, c);
	}

	for (i=0; i<64; i++)
	{
		pal_edit[0x10 + i] = pal_nes[i];
	}

	bmp_cur_draw = al_create_bitmap(10,10);
	al_set_target_bitmap(bmp_cur_draw);
	al_clear_to_color(al_map_rgba(0,0,0,0));
	al_draw_rectangle(4.5,0.5, 5.5,1.5, al_map_rgb(255,255,255), 1);
	al_draw_rectangle(0.5,4.5, 1.5,5.5, al_map_rgb(255,255,255), 1);
	al_draw_rectangle(8.5,4.5, 9.5,5.5, al_map_rgb(255,255,255), 1);
	al_draw_rectangle(4.5,8.5, 5.5,9.5, al_map_rgb(255,255,255), 1);
	al_draw_rectangle(1.5,1.5, 8.5,8.5, al_map_rgb(0,0,0), 1);
	al_draw_rectangle(2.5,2.5, 7.5,7.5, al_map_rgb(0,0,0), 1); // 239
	cur_draw = al_create_mouse_cursor(bmp_cur_draw, 4, 4);

	al_set_target_backbuffer(display);

	return 0;
}

int main(int argc, const char *argv[])
{
	bool wants_to_quit = false;
	int i;
	const char *filenamep = NULL;

	al_init();

	if (argc > 1)
		filenamep = argv[1];
	else if (select_file(filename))
		filenamep = filename;
	else
		return 1;

	do_init();

	rom = rom_init();
	if (!rom)
		return 1;

	if (rom_load(rom, filenamep))
		return 1;

	team = rom->team;

	while (!wants_to_quit && (i = select_team()) != -1)
	{		
		TEAM *t = &(team[i]);
		ALLEGRO_BITMAP *bmp_grid;
		ALLEGRO_EVENT event;
		ALLEGRO_EVENT_QUEUE *queue;
		ALLEGRO_TIMER *timer;

		int hide_grid = false;
		int hide_unused_tiles = true;
		
		GRID *grid = &(t->logo->grid);

		int active_layer = LAYER_LOGO;
		int pen_mode = PEN_OFF, pen_color = 0;
		int tool = TOOL_DRAW;

		int new_tile_num = -1, new_tile_x=0, new_tile_y=0;
		int replace_tile_index = -1;
		int move_type = -1;
		
		al_clear_to_color(al_map_rgb(0,0,0));

		for (i=0; i<8; i++)
		{
			pal_edit[0x50+i] = pal_nes[t->palette[i]];
		}
		active_pal = pal_edit;

		bmp_grid = al_create_bitmap(240,240);

		timer = al_create_timer(1);

		queue = al_create_event_queue();
		al_register_event_source(queue, al_get_keyboard_event_source());
		al_register_event_source(queue, al_get_mouse_event_source());
		al_register_event_source(queue, al_get_display_event_source(display));
		al_register_event_source(queue, al_get_timer_event_source(timer));

		al_start_timer(timer);

		while (true)
		{	
			al_clear_to_color(al_map_rgb(0,0,0));

			al_set_target_bitmap(bmp_grid);
			al_clear_to_color(al_map_rgba(0,0,0,0));

			for (i=0; i<grid->tile_count;i++)
			{
				GRID_TILE *gt = &(grid->tile[i]);
				al_draw_rectangle(
					(grid->x*4) + (gt->x*4) + gt->col*32 + 0.5,
					(grid->y*4) + (gt->y*4) + gt->row*32 + 0.5,
					(grid->x*4) + (gt->x*4) + gt->col*32 + 32 + 0.5,
					(grid->y*4) + (gt->y*4) + gt->row*32 + 32 + 0.5, 
					active_pal[250], 1
				);
			}

			al_set_target_backbuffer(display);	

			al_clear_to_color(al_map_rgb(0,0,0));

			al_draw_line(0, 80.5, 640, 80.5, active_pal[12], 1);
			al_draw_line(0, 420.5, 640, 420.5, active_pal[12], 1); 	
			al_draw_rectangle(18.5, 98.5, 277.5, 357.5, active_pal[255], 1);
			
			helmet_draw(rom->helmets[t->helmet_num - 0x40], rom->tiles + 40*4096, 0x4F, al_get_target_bitmap(), 20,100, 4);	
			draw_logo(t->logo, 0x54, al_get_target_bitmap(), 20, 100, 4);

			
			if (!hide_grid) al_draw_bitmap(bmp_grid, 20, 100, 0);

			// palette
			al_draw_textf(font, active_pal[255], 20, 380 - al_get_font_line_height(font) - 5, ALLEGRO_ALIGN_LEFT, "Helmet");
			for (i=0; i<3; i++)			
			{
				al_draw_filled_rectangle(20 + i * 24, 380, 20 + i * 24 + 23, 403, active_pal[0x50 + i]);
				if (active_layer == LAYER_HELMET && pen_color == i)
				{
					al_draw_rectangle(19.5 + i * 24, 379.5, 20.5 + i * 24 + 23, 403.5, active_pal[0x50 + i], 1);
					al_draw_rectangle(21.5 + i * 24, 381.5, 20.5 + i * 24 + 21, 401.5, active_pal[0], 1);
					al_draw_rectangle(31.5 + i * 24, 391.5, 31.5 + i * 24 + 1, 392.5, active_pal[255], 1);
				}
			}
			al_draw_rectangle(18.5, 378.5, 92.5, 404.5, active_pal[255], 1);

			al_draw_textf(font, active_pal[255], 100, 380 - al_get_font_line_height(font) - 5, ALLEGRO_ALIGN_LEFT, "Mask");
			for (i=0; i<2; i++)			
				al_draw_filled_rectangle(100 + i * 24, 380, 100 + i * 24 + 23, 403, active_pal[0x53 + i]);
			al_draw_rectangle(98.5, 378.5, 148.5, 404.5, active_pal[255], 1);

			al_draw_textf(font, active_pal[255], 156, 380 - al_get_font_line_height(font) - 5, ALLEGRO_ALIGN_LEFT, "Logo");
			for (i=0; i<3; i++)			
			{
				al_draw_filled_rectangle(156 + i * 24, 380, 156 + i * 24 + 23, 403, active_pal[0x55 + i]);
				if (active_layer == LAYER_LOGO && pen_color == i)
				{
					al_draw_rectangle(155.5 + i * 24, 379.5, 156 + i * 24 + 23.5, 403.5, active_pal[0x55 + i], 1);
					al_draw_rectangle(157.5 + i * 24, 381.5, 156 + i * 24 + 21.5, 401.5, active_pal[0], 1);
					al_draw_rectangle(167.5 + i * 24, 391.5, 167 + i * 24 + 1.5, 392.5, active_pal[255], 1);
				}
			}
			al_draw_rectangle(154.5, 378.5, 228.5, 404.5, active_pal[255], 1);

			al_draw_rectangle(236.5,378.5, 262.5,404.5, active_pal[255], 1);
			al_draw_textf(
				font,
				tool == TOOL_DRAW ? active_pal[15] : active_pal[255],
				249 - al_get_text_width(font, "D") / 2,
				390 - al_get_font_line_height(font) / 2,
				ALLEGRO_ALIGN_LEFT,
				"D"
			);
			al_draw_rectangle(264.5,378.5, 290.5,404.5, active_pal[255], 1);
			al_draw_textf(
				font,
				tool == TOOL_CLEAR ? active_pal[15] : active_pal[255],
				277 - al_get_text_width(font, "C") / 2,
				390 - al_get_font_line_height(font) / 2,						
				ALLEGRO_ALIGN_LEFT,
				"C"
			);
			al_draw_rectangle(292.5,378.5, 318.5,404.5, active_pal[255], 1);
			al_draw_textf(
				font,
				tool == TOOL_MOVE ? active_pal[15] : active_pal[255],
				305 - al_get_text_width(font, "M") / 2,
				390 - al_get_font_line_height(font) / 2,						
				ALLEGRO_ALIGN_LEFT,
				"M"
			);
			
			al_draw_rectangle(355.5, 138.5, 621.5, 404.5, active_pal[255], 1);

			for (i=0; i<grid->tile_count; i++)
			{
				GRID_TILE *gt = &(grid->tile[i]);
				al_draw_filled_rectangle(357 + (gt->num % 8) * 33, 140 + (gt->num / 8) * 33, 357 + (gt->num % 8) * 33 + 32, 140 + (gt->num / 8) * 33 + 32, active_pal[0x51]);
				if (hide_unused_tiles) 
					draw_gfx(t->logo->chr + gt->num*64, 8,8, 0, 0x54, al_get_target_bitmap(), 357+(gt->num%8)*33,140+(gt->num/8)*33, 4);
			}

			if (!hide_unused_tiles)
			{
				for (i=0; i<TILES_PER_PAGE; i++)
					draw_gfx(t->logo->chr + i*64, 8,8, 0, 0x54, al_get_target_bitmap(), 357+(i%8)*33,140+(i/8)*33, 4);
			}

			if (tool == TOOL_MOVE && new_tile_num != -1)
			{
				draw_gfx(t->logo->chr + new_tile_num*64, 8,8, 0, 0x54, al_get_target_bitmap(), new_tile_x,new_tile_y, 4);
				al_draw_rectangle(new_tile_x + 0.5, new_tile_y + 0.5, new_tile_x + 32.5, new_tile_y + 32.5, active_pal[0x50], 1);
			}
			
			al_flip_display();

			do
			{
				int mouse_x, mouse_y;

				al_wait_for_event(queue, &event);

				mouse_x = (event.mouse.x - offset_x) / scale_x;
				mouse_y = (event.mouse.y - offset_y) / scale_y;

				switch (event.type)
				{
					case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:						
						if (tool == TOOL_DRAW)
						{
							if (active_layer == LAYER_HELMET && between_coords(mouse_x,mouse_y, 20,100, 260,340))
							{
								if (event.mouse.button & 1) pen_mode = PEN_DRAW;
								else if (event.mouse.button & 2) pen_mode = PEN_ERASE;
							}
							if (active_layer == LAYER_LOGO && get_xy_coord_tile_num(grid, mouse_x, mouse_y, NULL, NULL, NULL) != -1)
							{
								if (event.mouse.button & 1) pen_mode = PEN_DRAW;
								else if (event.mouse.button & 2) pen_mode = PEN_ERASE;
							}
							goto horrible_hack_to_draw_on_mouse_down;
						}
						else if (tool == TOOL_MOVE)
						{
							int tile_index;
							int tile_num = get_xy_coord_tile_num(grid, mouse_x, mouse_y, NULL, NULL, &tile_index);
							if (tile_num != -1)
							{
								new_tile_num = tile_num;
								replace_tile_index = tile_index;

								if (tile_index != -1)
								{
									int c = (int)floor((((mouse_x - 20)/4.0) - grid->x) / 8.0);
									int r = (int)floor((((mouse_y - 100)/4.0) - grid->y) / 8.0);

									new_tile_x = 20 + (grid->x + grid->tile[tile_index].x + 8*c) * 4;
									new_tile_y = 100 + (grid->y + grid->tile[tile_index].y + 8*r) * 4;

									move_type = (event.mouse.z == 2) ? MOVE_OFFSET : MOVE_NORMAL;
								}
								else
								{
									new_tile_x = mouse_x;
									new_tile_y = mouse_y;

									move_type = MOVE_NORMAL;
								}
							}
						}
						break;

					case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
						if (between_coords(mouse_x, mouse_y, 236,378, 262,404))
						{
							tool = TOOL_DRAW;
						}
						else if (between_coords(mouse_x, mouse_y, 264,378, 290,404))
						{
							tool = TOOL_CLEAR;
						}
						else if (between_coords(mouse_x, mouse_y, 292,378, 318,404))
						{
							tool = TOOL_MOVE;
						}						
						else if (tool == TOOL_CLEAR)
						{
							int tile_num = get_xy_coord_tile_num(grid, mouse_x, mouse_y, NULL, NULL, NULL);
							if (tile_num != -1)
							{
								memset(t->logo->chr + tile_num*64, 0, 64);
							}
						}
						else
						{
							for (i=0; i<3; i++)
								if (between_coords(mouse_x,mouse_y, 20 + i*24,380, 20 + i*24+22,402))
								{
									active_pal[239] = pal_nes[t->palette[i]];
									active_layer = LAYER_HELMET;

									pen_color = i;
								}

							for (i=0; i<3; i++)
								if (between_coords(mouse_x,mouse_y, 156 + i*24,380, 156 + i*24+22,402))
								{
									active_pal[239] = pal_nes[t->palette[i+5]];
									active_layer = LAYER_LOGO;

									pen_color = i;
								}
						}

						if (tool == TOOL_DRAW)
						{
							pen_mode = PEN_OFF;
						}
						else if (tool == TOOL_MOVE)
						{
							if (new_tile_num != -1)
							{
								if (move_type == MOVE_NORMAL)
								{
									if (between_coords(mouse_x,mouse_y, 20,100, 260,340))
									{
										int row = (int)floor((((mouse_y - 100)/4.0) - grid->y) / 8.0);
										int col = (int)floor((((mouse_x - 20)/4.0) - grid->x) / 8.0);

										for (i=0; i<grid->tile_count; i++)
										{
											if (grid->tile[i].row == row && grid->tile[i].col == col) break;
										}

										if (replace_tile_index == -1)
										{	
											grid->tile[i].num = new_tile_num;
											grid->tile[i].x = 0;
											grid->tile[i].y = 0;
											grid->tile[i].row = row;
											grid->tile[i].col = col;									
											if (i == grid->tile_count) grid->tile_count++;
										}
										else if (replace_tile_index != i) // the tile was dropped where it began
										{											
											grid->tile[replace_tile_index].row = row;
											grid->tile[replace_tile_index].col = col;
											if (i < grid->tile_count)
											{
												grid->tile[i] = grid->tile[--grid->tile_count];
											}
										}
									}
									else if (replace_tile_index != -1 && between_coords(mouse_x,mouse_y, 356,139, 620,403))
									{
										grid->tile[replace_tile_index] = grid->tile[--grid->tile_count];
									}
								}
								else if (move_type == MOVE_OFFSET)
								{
									int x_os = ((mouse_x-20) / 4) - (grid->x + 8*grid->tile[replace_tile_index].col);
									int y_os = ((mouse_y-100) / 4) - (grid->y + 8*grid->tile[replace_tile_index].row);
									if (x_os < -7) x_os = -7;
										else if (x_os > 7) x_os = 7;
									if (y_os < -7) y_os = -7;
										else if (y_os > 7) y_os = 7;

									grid->tile[replace_tile_index].x = x_os;
									grid->tile[replace_tile_index].y = y_os;
								}
							}
							new_tile_num = -1;
						}
						
						break;

					case ALLEGRO_EVENT_MOUSE_AXES:
						if (tool == TOOL_DRAW)
						{
horrible_hack_to_draw_on_mouse_down: // see ALLEGRO_EVENT_MOUSE_BUTTON_DOWN from above
							if 	(						
								(active_layer == LAYER_HELMET && between_coords(mouse_x,mouse_y, 20,100, 260,340)) ||
								(active_layer == LAYER_LOGO && get_xy_coord_tile_num(grid, mouse_x,mouse_y, NULL, NULL, NULL) != -1)
								)						
							{
								al_set_mouse_cursor(display, cur_draw);
							}
							else
							{
								al_set_system_mouse_cursor(display, ALLEGRO_SYSTEM_MOUSE_CURSOR_DEFAULT);								
							}
					
							if (pen_mode == PEN_DRAW || pen_mode == PEN_ERASE)
							{
								if (active_layer == LAYER_HELMET && between_coords(mouse_x,mouse_y, 20,100, 260,340))
								{
									helmet_putpixel(rom->helmets[t->helmet_num - 0x40], rom->tiles + 40*4096, (mouse_x-20)/4,(mouse_y-100)/4, pen_mode == PEN_DRAW ? (pen_color+1) : 0);
								}
								else if (active_layer == LAYER_LOGO)
								{
									int x, y;
									int tile_num = get_xy_coord_tile_num(grid, mouse_x,mouse_y, &x, &y, NULL);
									if (tile_num != -1)
									{
										*(t->logo->chr + (tile_num * 64) + (y * 8) + x) = pen_mode == PEN_DRAW ? (pen_color+1) : 0;
									}
								}
							}
						}
						else if (tool == TOOL_MOVE)
						{
							if (new_tile_num != -1)
							{
								if (move_type == MOVE_NORMAL)
								{
									if (mouse_x >= 20 && mouse_x <= 260 && mouse_y >=100 && mouse_y <= 340)
									{
										int c = (int)floor((((mouse_x - 20)/4.0) - grid->x) / 8.0);
										int r = (int)floor((((mouse_y - 100)/4.0) - grid->y) / 8.0);

										if (replace_tile_index != -1)
										{ // moving a tile, let's remember the offsets
											new_tile_x = 20 + (grid->x + grid->tile[replace_tile_index].x + 8*c) * 4;
											new_tile_y = 100 + (grid->y + grid->tile[replace_tile_index].y + 8*r) * 4;										
										}
										else
										{
											new_tile_x = 20 + (grid->x + 8*c) * 4;
											new_tile_y = 100 + (grid->y + 8*r) * 4;
										}
									}
									else
									{
										new_tile_x = mouse_x;
										new_tile_y = mouse_y;
									}
								}
								else if (move_type == MOVE_OFFSET)
								{
									int x_os = ((mouse_x-20) / 4) - (grid->x + 8*grid->tile[replace_tile_index].col);
									int y_os = ((mouse_y-100) / 4) - (grid->y + 8*grid->tile[replace_tile_index].row);
									if (x_os < -7) x_os = -7;
										else if (x_os > 7) x_os = 7;
									if (y_os < -7) y_os = -7;
										else if (y_os > 7) y_os = 7;

									grid->tile[replace_tile_index].x = x_os;
									grid->tile[replace_tile_index].y = y_os;
									
									new_tile_x = 20 + (grid->x + grid->tile[replace_tile_index].x + 8*grid->tile[replace_tile_index].col ) * 4;
									new_tile_y = 100 + (grid->y + grid->tile[replace_tile_index].y + 8*grid->tile[replace_tile_index].row ) * 4;
								}
							}
						}
						
						break;

					case ALLEGRO_EVENT_DISPLAY_CLOSE:
						wants_to_quit = true;
					 	goto get_me_out_of_here;
						break;

					case ALLEGRO_EVENT_KEY_UP:
					{
						int k = event.keyboard.keycode;

						if (k == ALLEGRO_KEY_ESCAPE) goto get_me_out_of_here;
						switch (k)
						{
							case ALLEGRO_KEY_1:
								t->helmet_num = 0x40;
								break;

							case ALLEGRO_KEY_2:
								t->helmet_num = 0x41;
								break;

							case ALLEGRO_KEY_3:
								t->helmet_num = 0x42;
								break;

							case ALLEGRO_KEY_4:
								t->helmet_num = 0x43;
								break;

							case ALLEGRO_KEY_F10:
								al_clear_to_color(al_map_rgb(0,0,0));
								al_flip_display();
								al_toggle_display_flag(display, ALLEGRO_FULLSCREEN_WINDOW, !(al_get_display_flags(display) & ALLEGRO_FULLSCREEN_WINDOW));
								scale_screen(SCREEN_W, SCREEN_H, al_get_display_width(display), al_get_display_height(display));
								break;

							case ALLEGRO_KEY_F11:
								hide_grid = hide_grid ? false : true;
								break;

							case ALLEGRO_KEY_F12:
								hide_unused_tiles = hide_unused_tiles ? false : true;
								break;

							case ALLEGRO_KEY_PGDN:
								if (++t->logo->hdr.pt_num == 128) t->logo->hdr.pt_num = 0;
								t->logo->chr = rom->tiles + (t->logo->hdr.pt_num)*4096;
								break;

							case ALLEGRO_KEY_PGUP:
								if (--t->logo->hdr.pt_num == 255) t->logo->hdr.pt_num = 127;
								t->logo->chr = rom->tiles + (t->logo->hdr.pt_num)*4096;
								break;

							case ALLEGRO_KEY_LEFT:						
								grid->x -= 2;
								if (grid->x < -92) grid->x = 418;
								break;

							case ALLEGRO_KEY_RIGHT:						
								grid->x += 2;
								if (grid->x > 418) grid->x = -92;
								break;

							case ALLEGRO_KEY_UP:
								grid->y -= 2;
								if (grid->y < -94) grid->y = 416;
								break;

							case ALLEGRO_KEY_DOWN:
								grid->y += 2;
								if (grid->y > 416) grid->y = -94;
								break;
						}
					}
				}
			}
			while (!al_is_event_queue_empty(queue));
		}
get_me_out_of_here:

		al_destroy_event_queue(queue);
		al_destroy_bitmap(bmp_grid);
		al_destroy_timer(timer);
		
		pic_grid_to_code(t->logo);

		fade_out();
	}

	for (i=0; i<rom->team_count; i++)
	{
		al_destroy_bitmap(team[i].bmp_helmet);
	}
	
	al_destroy_bitmap(bmp_cur_draw);

	rom_save(rom);
	rom_destroy(rom);

	free(init_dir);

	return 0;
}
