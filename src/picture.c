#include <stdio.h>
#include <stdlib.h>

#include "picture.h"
#include "rom.h"

void pic_code_to_grid(PIC *picture)
{
	int x=0,y=0,col=0,row=0;
	int i=0, j, k;
	GRID *grid = &(picture->grid);
	
	i = 0;			
	grid->tile_count = 0;
	grid->x = (picture->hdr.x-46)*2;
	grid->y = (picture->hdr.y-46)*2;

	while ((j = picture->data[i++]) != 0xff)
	{		
		if (j & 128)
		{			
			if (j == 0xFC)
			{
				k = picture->data[i] & 15;						
				if (k & 8) k -= 16;
				if (picture->data[i++] & 16)						
					x = k;
				else
					y = k;
			}
			else // move column						
			{
				j &= 31;
				if (j & 16) j -= 32;
				col = j;
			}
		}
		else
		{
			int tnum = picture->data[i++];					

			// TODO: check for flip
			j = (j&0x3C) >> 2;					
			if (j & 8) j -=16;
			row = j;

			if (tnum >= TILES_PER_PAGE){
				printf("\nPanic: tile num of %d\n", tnum);
				continue;
				tnum = 0;						
			}

            grid->tile[grid->tile_count].x = x;					
			grid->tile[grid->tile_count].y = y;
			grid->tile[grid->tile_count].row = row;
			grid->tile[grid->tile_count].col = col;
			grid->tile[grid->tile_count].num = tnum;

			grid->tile_count++;

		}
	}
}

int cmp(const void *a, const void *b)
{
	const GRID_TILE *ta = (GRID_TILE *)a;
	const GRID_TILE *tb = (GRID_TILE *)b;

	if ((ta->x == 0 && ta->y == 0) && (tb->x != 0 || tb->y != 0))
		return -1;
	else if ((tb->x == 0 && tb->y == 0) && (ta->x != 0 || ta->y != 0))
        return 1;

	else if ((ta->x == 0 || ta->y == 0) && (tb->x != 0 && tb->y != 0))
		return -1;
	else if ((tb->x == 0 || tb->y == 0) && (ta->x != 0 && ta->y != 0))
        return 1;

	else if (ta->x < tb->x)
		return -1;
	else if (ta->x > tb->x)
		return 1;
	else if (ta->y < tb->y)
		return -1;
	else if (ta->y > tb->y)
		return 1;	


	else if (ta->col < tb->col)
		return -1;
	else if (ta->col > tb->col)
		return 1;
	else if (ta->row < tb->row)
		return -1;
	else if (ta->row > tb->row)
		return 1;	


	return 0;
}

void pic_grid_to_code(PIC *pic)
{
	GRID *grid = &(pic->grid);
	int i;

	int col = 0xDEADBEEF;
	int x = 0, y = 0;
	int j=0;

	printf("Before:\n");
	for (i=0; i<grid->tile_count; i++)
		printf("%d: %d,%d (%d,%d) = %0x\n", i, grid->tile[i].col,grid->tile[i].row,grid->tile[i].x,grid->tile[i].y,grid->tile[i].num);

	i=0;
	printf ("\n");
	while (pic->data[i] != 0xFF) printf("%02x ", pic->data[i++]);
	printf ("\nSize: %d\n\n", i);
	
	qsort(&(grid->tile), grid->tile_count, sizeof(GRID_TILE), cmp);

	for (i=0; i<grid->tile_count; i++)
	{
		GRID_TILE *t = &(grid->tile[i]);

		if (t->x != x)
		{
			int b = t->x;
			if (b < 0)
			{
				b += 16;
				b &= 7;
				b |= 8;
			}
			else
			{
				b &= 7;
			}
			b |= (32+16);

			pic->data[j++] = 0xFC;
			pic->data[j++] = b;
			x = t->x;
		}
		if (t->y != y)
		{
			int b = t->y;
			if (b < 0)
			{
				b += 16;
				b &= 7;
				b |= 8;
			}
			else
			{
				b &= 7;
			}
			b |= 32;

			pic->data[j++] = 0xFC;
			pic->data[j++] = b;
			y = t->y;
		}

		if (t->col != col)
		{
			int b = t->col;
			if (b < 0) 
			{
				b += 32;
				b &= 15;
				b |= 16;
			}
			else
			{
				b &= 15;
			}
			b |= 128;

			pic->data[j++] = b;		
			col = t->col;
		}
		
		{
			int b = t->row;
			if (b < 0)
			{
				b += 16;
				b &= 7;
				b |= 8;
			}
			else
			{
				b &= 7;
			}

			b <<= 2;
			b |= 1;
			
			pic->data[j++] = b;
			pic->data[j++] = t->num;
		}
	}
	pic->data[j++] = 0xFF;

	pic->hdr.x = (grid->x / 2) + 46;
	pic->hdr.y = (grid->y / 2) + 46;

	printf("After:\n");
	for (i=0; i<grid->tile_count; i++)
		printf("%d: %d,%d (%d,%d) = %0x\n", i, grid->tile[i].col,grid->tile[i].row,grid->tile[i].x,grid->tile[i].y,grid->tile[i].num);
	
	i=0;
	printf("\n");
	while (pic->data[i] != 0xFF) printf("%02x ", pic->data[i++]);
	printf ("\nSize: %d\n\n", i);

	printf("------------------\n");
}