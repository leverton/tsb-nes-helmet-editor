ALLEGRO_VERSION=5.0
CC = gcc
OBJDIR = obj
CFLAGS = -O2 -W -Wall -Iinclude 
LFLAGS = `pkg-config --libs allegro-$(ALLEGRO_VERSION) allegro_primitives-$(ALLEGRO_VERSION) allegro_font-$(ALLEGRO_VERSION) allegro_ttf-$(ALLEGRO_VERSION) allegro_dialog-$(ALLEGRO_VERSION)`

tsb-helmet-editor: $(OBJDIR)/gfx.o $(OBJDIR)/rom.o $(OBJDIR)/team.o $(OBJDIR)/picture.o $(OBJDIR)/helmet.o
	$(CC) $(OBJDIR)/*.o -o tsb-helmet-editor $(LFLAGS)

$(OBJDIR)/gfx.o: src/gfx.c
	$(CC) $(CFLAGS) -c src/gfx.c -o $(OBJDIR)/gfx.o

$(OBJDIR)/rom.o: src/rom.c
	$(CC) $(CFLAGS) -c src/rom.c -o $(OBJDIR)/rom.o

$(OBJDIR)/team.o: src/team.c
	$(CC) $(CFLAGS) -c src/team.c -o $(OBJDIR)/team.o

$(OBJDIR)/picture.o: src/picture.c
	$(CC) $(CFLAGS) -c src/picture.c -o $(OBJDIR)/picture.o

$(OBJDIR)/helmet.o: src/helmet.c
	$(CC) $(CFLAGS) -c src/helmet.c -o $(OBJDIR)/helmet.o


clean:
	-rm $(OBJDIR)/*.o
	-rm tsb-helmet-editor
