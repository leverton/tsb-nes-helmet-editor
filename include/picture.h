#ifndef PICTURE_H
#define PICTURE_H

#include <allegro5/allegro.h>

typedef struct GRID_TILE {
	int x, y; // tile offset
	int row, col; // row/col position of tile
	int num; // tile number (0 to 63)
} GRID_TILE;

typedef struct GRID {
	int x, y; // grid offset
	int tile_count; // number of tiles (max of 64)
	GRID_TILE tile[64];
} GRID;

typedef struct PIC_HDR {
	unsigned char junk0;
	unsigned char pt_num; 
	unsigned char junk1[6];
	unsigned char y, x;
	uint16_t design_addr; 
	unsigned char junk2[2];
} PIC_HDR;

typedef struct PIC {
	int hdr_addr;
	PIC_HDR hdr;
	GRID grid;

	unsigned char *chr;

	unsigned char data[2048];

	struct PIC *next;
} PIC;


void pic_code_to_grid(PIC *p);
void pic_grid_to_code(PIC *p);


#endif