#ifndef ROM_H
#define ROM_H

#include "team.h"
#include "helmet.h"
#include "picture.h"


#define TILE_PAGES 64
#define TILES_PER_PAGE 64
#define TILE_H 8
#define TILE_W 8

typedef struct ROM
{
	char *filename;
	unsigned char *tiles;	

	PIC *pictures;
	HELMET *helmets[4];

	TEAM *team;
	int team_count;
} ROM;

ROM *rom_init(void);
void rom_destroy(ROM *rom);

int rom_load(ROM *rom, const char *filename);
void rom_save(ROM *rom);

PIC *rom_add_picture(ROM *rom);

#endif