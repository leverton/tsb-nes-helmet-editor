#ifndef GFX_H_
#define GFX_H_

void draw_gfx(const unsigned char *img, int w, int h, unsigned char mask_c, int base_c, ALLEGRO_BITMAP *bmp, int x, int y, int zoom);

#endif