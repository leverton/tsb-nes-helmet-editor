#ifndef TEAM_H
#define TEAM_H

#include <allegro5/allegro.h>
#include "picture.h"

typedef struct TEAM {
	char name[5];
	unsigned char palette[8];
	int address;
	int num;	
	PIC *logo;	
	ALLEGRO_BITMAP *bmp_helmet;
	unsigned char helmet_num;
} TEAM;

#endif