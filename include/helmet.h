#ifndef HELMET_H_
#define HELMET_H_

#include <allegro5/allegro.h>

typedef struct GFX_GRID {
	unsigned char *data;	
	int w,h;
	int address;
	
	struct GFX_GRID *next;
} GFX_GRID;

typedef struct GFX_GRID_LINK {	
	int x,y;

	GFX_GRID *grid;
	
	struct GFX_GRID_LINK *next;
} GFX_GRID_LINK;

typedef struct HELMET {
	GFX_GRID_LINK *grid_links;
} HELMET;

HELMET *helmet_create(void);
void helmet_destroy(HELMET *helmet);
void helmet_add_grid(HELMET *helmet, GFX_GRID *grid, int x, int y);
void helmet_draw(HELMET *helmet, unsigned char *tiles, int base_c, ALLEGRO_BITMAP *bmp, int dest_x, int dest_y, int zoom);
bool helmet_putpixel(HELMET *helmet, unsigned char *tiles, int x, int y, char c);

GFX_GRID *gfx_grid_create(int address, int w, int h);
void gfx_grid_load_all(ALLEGRO_FILE *fp);
void gfx_grid_clear(void);

#endif
